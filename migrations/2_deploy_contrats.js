// var BGPSHARE = artifacts.require("BGPPrefixesManagerContract.sol");

// module.exports = function(deployer) {
//   deployer.deploy(BGPSHARE);
// };

var RIR = artifacts.require("RIRManagerContract.sol");

module.exports = function(deployer) {
  const contractAddress = "0xf8F78b24819b0219aF51905DE88Adb39a560a296";
  const nameRIR = "RIRTEST";
  deployer.deploy(RIR, nameRIR, contractAddress);
};