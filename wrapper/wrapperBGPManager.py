from __future__ import with_statement, print_function
import argparse
import json
import hashlib
import time
from configs import BGPConf 
from base64 import b64encode
from Crypto.Cipher import AES
from Crypto.Util.Padding import pad
from Crypto import Random
from solcx import compile_files
from web3 import Web3, HTTPProvider
from web3.gas_strategies.time_based import fast_gas_price_strategy

contract=""
typeOfMemberDict = {"RIR": 0, "LIR": 1, "ISP": 2}
web3 = Web3(Web3.HTTPProvider(BGPConf.infuraURL))
web3.eth.defaultAccount = Web3.toChecksumAddress(BGPConf.bgpAccount)
addressContract = Web3.toChecksumAddress(BGPConf.addressContract)

def _compile_source_file(file_path):
   with open(file_path, 'r') as f:
      source = f.read()

   return compile_files([source, '../contracts/LIRContract.sol'])

def _getPK():
    with open('../secrets.js') as secrets:
        line = secrets.readline()
        while line:
            if 'accountPK' in line:
                return  Web3.toHex(hexstr=line[line.find('=')+3:-3])
            line = secrets.readline()
    return None

def _intToHex(network):
    network, mask = network.split('/')
    ipHex = ""
    for i in network.split('.'):
	    ipHex += str('{0:0{1}X}'.format(int(i),2))

    netHex = "0x" +ipHex + str('{0:0{1}X}'.format(int(mask),2))

    return netHex

def addRIRorLIR(memberAddress, typeOfMember):
    nonce = web3.eth.getTransactionCount(web3.eth.defaultAccount)
    checksum_address = web3.toChecksumAddress(memberAddress)

    web3.eth.setGasPriceStrategy(fast_gas_price_strategy)
    gasprice = web3.eth.generateGasPrice()

    txn_dict = contract.functions.addRIRorLIR(checksum_address, typeOfMemberDict[typeOfMember]).buildTransaction({
        'chainId': 3,
        'gasPrice': gasprice,
        'nonce': nonce,
    })

    signed_txn = web3.eth.account.signTransaction(txn_dict, private_key=_getPK())
    result = web3.eth.sendRawTransaction(signed_txn.rawTransaction)

    print("Transaction hash: " + web3.toHex(result))

    tx_receipt = web3.eth.waitForTransactionReceipt(result)

    if tx_receipt is None:
        return ("Error: timeout")    
    else:
        if tx_receipt['status']:
            return "The transaction is a success."
        else:
            return "The transaction is a fail."

def isMember(memberAddress, typeOfMember):
    checksum_address = Web3.toChecksumAddress(memberAddress)

    return contract.functions.isMember(checksum_address, typeOfMemberDict[typeOfMember]).call()

def addISP(memberAddress, asNumber):
    nonce = web3.eth.getTransactionCount(web3.eth.defaultAccount)
    checksum_address = web3.toChecksumAddress(memberAddress)
    
    web3.eth.setGasPriceStrategy(medium_gas_price_strategy)
    gasprice = web3.eth.generateGasPrice()
    
    txn_dict = contract.functions.addISP(checksum_address, asNumber).buildTransaction({
        'chainId': 3, 
        'gasPrice': gasprice,
        'nonce': nonce,
    })

    signed_txn = web3.eth.account.signTransaction(txn_dict, private_key=_getPK())
    result = web3.eth.sendRawTransaction(signed_txn.rawTransaction)

    print("Transaction hash: " + web3.toHex(result))

    tx_receipt = web3.eth.waitForTransactionReceipt(result)

    if tx_receipt is None:
        return ("Error: timeout")    
    else:
        if tx_receipt['status']:
            return "The transaction is a success."
        else:
            return "The transaction is a fail."

def removeMember(memberAddress, typeOfMember):
    nonce = web3.eth.getTransactionCount(web3.eth.defaultAccount)

    web3.eth.setGasPriceStrategy(fast_gas_price_strategy)
    gasprice = web3.eth.generateGasPrice()

    txn_dict = contract.functions.removeMember(memberAddress, typeOfMemberDict[typeOfMember]).buildTransaction({
        'chainId': 3,
        'gasPrice': gasprice,
        'nonce': nonce,
    })

    signed_txn = web3.eth.account.signTransaction(txn_dict, private_key=_getPK())
    result = web3.eth.sendRawTransaction(signed_txn.rawTransaction)

    print("Transaction hash: " + web3.toHex(result))

    tx_receipt = web3.eth.waitForTransactionReceipt(result)

    if tx_receipt is None:
        return ("Error: timeout")    
    else:
        if tx_receipt['status']:
            return "The transaction is a success."
        else:
            return "The transaction is a fail."

def getROA(network):
    return contract.functions.getROA(_intToHex(network)).call()

def addDeviceConfiguration(asNumber, idRouter, networks):
    networksList = []

    for network in networks.split(','):
        networksList.append(_intToHex(network))

    nonce = web3.eth.getTransactionCount(web3.eth.defaultAccount)
    
    web3.eth.setGasPriceStrategy(medium_gas_price_strategy)
    gasprice = web3.eth.generateGasPrice()
    
    txn_dict = contract.functions.addDeviceConfiguration(asNumber,idRouter, networksList).buildTransaction({
        'chainId': 3, 
        'gasPrice': gasprice,
        'nonce': nonce,
    })

    signed_txn = web3.eth.account.signTransaction(txn_dict, private_key=_getPK())
    result = web3.eth.sendRawTransaction(signed_txn.rawTransaction)

    print("Transaction hash: " + web3.toHex(result))

    tx_receipt = web3.eth.waitForTransactionReceipt(result)

    if tx_receipt is None:
        return ("Error: timeout")    
    else:
        if tx_receipt['status']:
            return "The transaction is a success."
        else:
            return "The transaction is a fail."

def delDeviceConfiguration(asNumber, idRouter):
    nonce = web3.eth.getTransactionCount(web3.eth.defaultAccount)
    
    web3.eth.setGasPriceStrategy(medium_gas_price_strategy)
    gasprice = web3.eth.generateGasPrice()
    
    txn_dict = contract.functions.delDeviceConfiguration(asNumber,idRouter).buildTransaction({
        'chainId': 3, 
        'gasPrice': gasprice,
        'nonce': nonce,
    })

    signed_txn = web3.eth.account.signTransaction(txn_dict, private_key=_getPK())
    result = web3.eth.sendRawTransaction(signed_txn.rawTransaction)

    print("Transaction hash: " + web3.toHex(result))

    tx_receipt = web3.eth.waitForTransactionReceipt(result)

    if tx_receipt is None:
        return ("Error: timeout")    
    else:
        if tx_receipt['status']:
            return "The transaction is a success."
        else:
            return "The transaction is a fail."

def getDeviceConfiguration(asNumber, idRouter):
    return contract.functions.getDeviceConfiguration(asNumber,idRouter).call()
    
if __name__=="__main__": 
    compiled_sol = _compile_source_file("../contracts/BGPPrefixesManagerContract.sol")
    contract_id, contract_interface = compiled_sol.popitem()
    contract_id, contract_interface = compiled_sol.popitem()
    contract = web3.eth.contract(address=web3.toChecksumAddress(BGPConf.addressContract),abi=contract_interface['abi'])

    parser = argparse.ArgumentParser(prog= 'wrapperBGPManager.py', description= 'It is a wrapper to interact with a smart contract for .')
    parser.add_argument('-a', '--addRL', nargs=2, help='', metavar=('PUBLIC ADDRESS,','Type of Member'))
    parser.add_argument('-p', '--addISP', nargs=2, help='', metavar=('PUBLIC ADDRESS,', 'AS Number'))
    parser.add_argument('-r', '--rmMember', nargs=2, help='', metavar=('PUBLIC ADDRESS,','Type of Member'))
    parser.add_argument('-i', '--isMember', nargs=2, help='', metavar=('PUBLIC ADDRESS,','Type of Member'))
    parser.add_argument('-o', '--getROA', nargs=1, help='', metavar=('Network'))
    parser.add_argument('-c', '--addConf', nargs=3, help='', metavar=('AS Number,','ID Router,','List of Networks'))
    parser.add_argument('-d', '--delConf', nargs=2, help='', metavar=('AS Number,','ID Router'))
    parser.add_argument('-g', '--getConf', nargs=2, help='', metavar=('AS Number,','ID Router'))
    args = parser.parse_args()

    if args.addRL:
        if web3.isAddress(args.addRL[0]):
            print(addRIRorLIR(args.addRL[0] ,args.addRL[1]))
        else:
            print('Bad address.')
    elif args.addISP:
        if web3.isAddress(args.addISP[0]):
            print(addISP(args.addISP[0], args.addISP[1]))
        else:
            print('Bad address.')
    elif args.rmMember:
        if web3.isAddress(args.rmMember[0]):
            print(removeMember(args.rmMember[0], args.rmMember[1]))
        else:
            print('Bad address.')
    elif args.isMember:
       if web3.isAddress(args.isMember[0]):
        print(isMember(args.isMember[0].lower(), args.isMember[1]))
       else:
           print('Bad address.')
    elif args.getROA:
        print(getROA(args.getROA[0]))
    elif args.addConf:
        print(addDeviceConfiguration(args.addConf[0], args.addConf[1], args.addConf[2]))
    elif args.delConf:
        print(delDeviceConfiguration(args.delConf[0], args.delConf[1]))
    elif args.getConf:
        print(getDeviceConfiguration(args.getConf[0], args.getConf[1]))
        