from __future__ import with_statement, print_function
import argparse
import json
import hashlib
import time
import sys
from configs import LIRConf 
from base64 import b64encode
from Crypto.Cipher import AES
from Crypto.Util.Padding import pad
from Crypto import Random
from solcx import compile_files
from web3 import Web3, HTTPProvider
from web3.gas_strategies.time_based import fast_gas_price_strategy

contract=""
web3 = Web3(Web3.HTTPProvider(LIRConf.infuraURL))
web3.eth.defaultAccount = Web3.toChecksumAddress(LIRConf.lirAccount)
addressContract = Web3.toChecksumAddress(LIRConf.addressContract)

def _compile_source_file(file_path):
   with open(file_path, 'r') as f:
      source = f.read()

   return compile_files([source, '../contracts/BGPPrefixesManagerContract.sol'])

def _getPK():
    with open('../secrets.js') as secrets:
        line = secrets.readline()
        while line:
            if 'lirPK' in line:
                return  Web3.toHex(hexstr=line[line.find('=')+3:-3])
            line = secrets.readline()
    return None

def _intToHex(network):
    network, mask = network.split('/')
    ipHex = ""
    for i in network.split('.'):
	    ipHex += str('{0:0{1}X}'.format(int(i),2))

    netHex = "0x" +ipHex + str('{0:0{1}X}'.format(int(mask),2))

    return netHex

def getPrefixes():
    return contract.functions.getPrefixes().call()

def getOwner():
    return contract.functions.getOwner().call()

def setROA(asNumber, prefix, maxLen, period):
    nonce = web3.eth.getTransactionCount(web3.eth.defaultAccount)
    asNumber = Web3.toInt(int(asNumber))
    maxLen = Web3.toInt(int(maxLen))
    period = Web3.toInt(int(period))

    web3.eth.setGasPriceStrategy(fast_gas_price_strategy)
    gasprice = web3.eth.generateGasPrice()

    txn_dict = contract.functions.setROA(asNumber, _intToHex(prefix), maxLen, period).buildTransaction({
        'chainId': 3,
        'gasPrice': gasprice,
        'nonce': nonce,
    })

    signed_txn = web3.eth.account.signTransaction(txn_dict, private_key=_getPK())
    result = web3.eth.sendRawTransaction(signed_txn.rawTransaction)

    print("Transaction hash: " + web3.toHex(result))

    tx_receipt = web3.eth.waitForTransactionReceipt(result, timeout=180)

    if tx_receipt is None:
        return ("Error: timeout")    
    else:
        if tx_receipt['status']:
            return "The transaction is a success."
        else:
            return "The transaction is a fail."


def removeROA(asNumber, network, maxLen):
    nonce = web3.eth.getTransactionCount(web3.eth.defaultAccount)

    asNumber = Web3.toInt(int(asNumber))
    maxLen = Web3.toInt(int(maxLen))
    period = Web3.toInt(int(period))

    web3.eth.setGasPriceStrategy(fast_gas_price_strategy)
    gasprice = web3.eth.generateGasPrice()

    txn_dict = contract.functions.removeROA(asNumebr, _intToHex(prefix), maxLen).buildTransaction({
        'chainId': 3,
        'gasPrice': gasprice,
        'nonce': nonce,
    })

    signed_txn = web3.eth.account.signTransaction(txn_dict, private_key=_getPK())
    result = web3.eth.sendRawTransaction(signed_txn.rawTransaction)

    print("Transaction hash: " + web3.toHex(result))

    tx_receipt = web3.eth.waitForTransactionReceipt(result)

    if tx_receipt is None:
        return ("Error: timeout")    
    else:
        if tx_receipt['status']:
            return "The transaction is a success."
        else:
            return "The transaction is a fail."

def addISP(addressISP, asNumber ):
    nonce = web3.eth.getTransactionCount(web3.eth.defaultAccount)
    checksum_address = web3.toChecksumAddress(addressISP)

    web3.eth.setGasPriceStrategy(fast_gas_price_strategy)
    gasprice = web3.eth.generateGasPrice()

    asNumber = Web3.toInt(int(asNumber))

    txn_dict = contract.functions.addISP(checksum_address, asNumber).buildTransaction({
        'chainId': 3,
        'gasPrice': gasprice,
        'nonce': nonce,
    })

    signed_txn = web3.eth.account.signTransaction(txn_dict, private_key=_getPK())
    result = web3.eth.sendRawTransaction(signed_txn.rawTransaction)

    print("Transaction hash: " + web3.toHex(result))

    tx_receipt = web3.eth.waitForTransactionReceipt(result)

    if tx_receipt is None:
        return ("Error: timeout")    
    else:
        if tx_receipt['status']:
            return "The transaction is a success."
        else:
            return "The transaction is a fail."

if __name__=="__main__": 
    compiled_sol = _compile_source_file("../contracts/LIRContract.sol")
    contract_id, contract_interface = compiled_sol.popitem()

    contract = web3.eth.contract(address=web3.toChecksumAddress(LIRConf.addressContract),abi=contract_interface['abi'])

    parser = argparse.ArgumentParser(prog= 'wrapperRIR.py', description= 'It is a wrapper to interact with a smart contract for .')
    parser.add_argument('-s', '--setROA', nargs=4, help='', metavar=('AS Number,','Prefix,', 'Max Length,', 'Period'))
    parser.add_argument('-r', '--removeROA', nargs=3, help='', metavar=('AS Number,','Prefix,', 'Max Length'))
    parser.add_argument('-a', '--addISP', nargs=2, help='', metavar=('PUBLIC ADDRESS of ISP,', 'AS Number'))
    parser.add_argument('-o', '--getOwner', action='store_true', help='')
    parser.add_argument('-p', '--getPrefix', action='store_true', help='')
    args = parser.parse_args()

    if args.setROA:
        print(setROA(args.setROA[0], args.setROA[1], args.setROA[2], args.setROA[3]))
    elif args.removeROA:
        print(removeROA(args.removeROA[0], args.removeROA[1], args.removeROA[2]))
    elif args.addISP:
        if web3.isAddress(args.addISP[0]):
            print(addISP(args.addISP[0], args.addISP[1]))
        else:
            print('Bad address.')
    elif args.getOwner:
        print(getOwner())
    elif args.getPrefix:
        print(getPrefixes())
        