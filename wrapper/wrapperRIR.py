from __future__ import with_statement, print_function
import argparse
import json
import hashlib
import time
from configs import RIRConf 
from base64 import b64encode
from Crypto.Cipher import AES
from Crypto.Util.Padding import pad
from Crypto import Random
from solcx import compile_files
from web3 import Web3, HTTPProvider
from web3.gas_strategies.time_based import fast_gas_price_strategy

contract=""
typeOfMemberDict = {"RIR": 0, "LIR": 1, "IPS": 3}
web3 = Web3(Web3.HTTPProvider(RIRConf.infuraURL))
web3.eth.defaultAccount = Web3.toChecksumAddress(RIRConf.rirAccount)
addressContract = Web3.toChecksumAddress(RIRConf.addressContract)

def _compile_source_file(*files_path):
   return compile_files(files_path)

def _getPK():
    with open('../secrets.js') as secrets:
        line = secrets.readline()
        while line:
            if 'rirPK' in line:
                return  Web3.toHex(hexstr=line[line.find('=')+3:-3])
            line = secrets.readline()
    return None

def _intToHex(network):
    network, mask = network.split('/')
    ipHex = ""
    for i in network.split('.'):
	    ipHex += str('{0:0{1}X}'.format(int(i),2))

    netHex = "0x" +ipHex + str('{0:0{1}X}'.format(int(mask),2))

    return netHex

def addPrefixPool(prefix):
    nonce = web3.eth.getTransactionCount(web3.eth.defaultAccount)

    web3.eth.setGasPriceStrategy(fast_gas_price_strategy)
    gasprice = web3.eth.generateGasPrice()

    txn_dict = contract.functions.addPrefixPool(_intToHex(prefix)).buildTransaction({
        'chainId': 3,
        'gasPrice': gasprice,
        'nonce': nonce,
    })

    signed_txn = web3.eth.account.signTransaction(txn_dict, private_key=_getPK())
    result = web3.eth.sendRawTransaction(signed_txn.rawTransaction)

    print("Transaction hash: " + web3.toHex(result))

    tx_receipt = web3.eth.waitForTransactionReceipt(result)

    if tx_receipt is None:
        return ("Error: timeout")    
    else:
        if tx_receipt['status']:
            return "The transaction is a success."
        else:
            return "The transaction is a fail."

def delPrefixPool(prefix):
    nonce = web3.eth.getTransactionCount(web3.eth.defaultAccount)

    web3.eth.setGasPriceStrategy(fast_gas_price_strategy)
    gasprice = web3.eth.generateGasPrice()

    txn_dict = contract.functions.delPrefixPool(_intToHex(prefix)).buildTransaction({
        'chainId': 3,
        'gasPrice': gasprice,
        'nonce': nonce,
    })

    signed_txn = web3.eth.account.signTransaction(txn_dict, private_key=_getPK())
    result = web3.eth.sendRawTransaction(signed_txn.rawTransaction)

    print("Transaction hash: " + web3.toHex(result))

    tx_receipt = web3.eth.waitForTransactionReceipt(result)

    if tx_receipt is None:
        return ("Error: timeout")    
    else:
        if tx_receipt['status']:
            return "The transaction is a success."
        else:
            return "The transaction is a fail."

def getPrefixPool(prefix):
    return contract.functions.getPrefixPool(_intToHex(prefix)).call()

def createLIRContract(addressLIR, nameLIR, prefixes):
    prefixesList = []

    for prefix in prefixes.split(','):
        prefixesList.append(_intToHex(prefix))
    
    nonce = web3.eth.getTransactionCount(web3.eth.defaultAccount)
    checksum_address = web3.toChecksumAddress(addressLIR)

    web3.eth.setGasPriceStrategy(fast_gas_price_strategy)
    gasprice = web3.eth.generateGasPrice()

    txn_dict = contract.functions.createLIRContract(checksum_address, nameLIR, prefixesList).buildTransaction({
        'chainId': 3,
        'gasPrice': gasprice,
        'nonce': nonce,
    })

    signed_txn = web3.eth.account.signTransaction(txn_dict, private_key=_getPK())
    result = web3.eth.sendRawTransaction(signed_txn.rawTransaction)

    print("Transaction hash: " + web3.toHex(result))

    tx_receipt = web3.eth.waitForTransactionReceipt(result)

    if tx_receipt is None:
        return ("Error: timeout")    
    else:
        if tx_receipt['status']:
            return "The transaction is a success."
        else:
            return "The transaction is a fail."

def getLIRContract(addressLIR):
    checksum_address = web3.toChecksumAddress(addressLIR)

    return contract.functions.getLIRContract(checksum_address).call()
    
if __name__=="__main__": 
    compiled_sol = _compile_source_file("../contracts/RIRManagerContract.sol", "../contracts/RIRManagerContract.sol")
    contract_id, contract_interface = compiled_sol.popitem()

    contract = web3.eth.contract(address=web3.toChecksumAddress(RIRConf.addressContract),abi=contract_interface['abi'])

    parser = argparse.ArgumentParser(prog= 'wrapperRIR.py', description= 'It is a wrapper to interact with a smart contract for .')
    parser.add_argument('-c', '--crCon', nargs=3, help='', metavar=('PUBLIC ADDRESS of LIR,','LIR Name,', 'PREFIXES'))
    parser.add_argument('-l', '--getLIR', nargs=1, help='', metavar=('PUBLIC ADDRESS of LIR,'))
    parser.add_argument('-a', '--addPR', nargs=1, help='', metavar=('PREFIX'))
    parser.add_argument('-d', '--delPR', nargs=1, help='', metavar=('PREFIX'))
    parser.add_argument('-p', '--getPR', nargs=1, help='', metavar=('PREFIX'))
    args = parser.parse_args()

    if args.crCon:
        if web3.isAddress(args.crCon[0]):
            print(createLIRContract(args.crCon[0], args.crCon[1], args.crCon[2]))
        else:
            print('Bad address.')
    elif args.getLIR:
        if web3.isAddress(args.getLIR[0]):
            print(getLIRContract(args.getLIR[0]))
        else:
            print('Bad address.')
    elif args.addPR:
        print(addPrefixPool(args.addPR[0]))
    elif args.delPR:
        print(delPrefixPool(args.delPR[0]))
    elif args.getPR:
        print(getPrefixPool(args.getPR[0]))
        