from __future__ import with_statement, print_function
import argparse
import json
import hashlib
import time
import sys
import web3
from configs import ISPConf 
from base64 import b64encode
from Crypto.Cipher import AES
from Crypto.Util.Padding import pad
from Crypto import Random
from solcx import compile_files
from web3 import Web3, HTTPProvider
from web3.gas_strategies.time_based import fast_gas_price_strategy

contract=""
web3 = Web3(Web3.WebsocketProvider(ISPConf.infuraURL))
web3.eth.defaultAccount = Web3.toChecksumAddress(ISPConf.ispAccount)
addressContract = Web3.toChecksumAddress(ISPConf.addressContract)

def _getPK():
    with open('../secrets.js') as secrets:
        line = secrets.readline()
        while line:
            if 'ispPK' in line:
                return  Web3.toHex(hexstr=line[line.find('=')+3:-3])
            line = secrets.readline()
    return None

def _compile_source_file(file_path):
   with open(file_path, 'r') as f:
      source = f.read()

   return compile_files([source, '../contracts/LIRContract.sol'])

def _intToHex(network):
    network, mask = network.split('/')
    ipHex = ""
    for i in network.split('.'):
	    ipHex += str('{0:0{1}X}'.format(int(i),2))

    netHex = "0x" +ipHex + str('{0:0{1}X}'.format(int(mask),2))

    return netHex

def addDeviceConfiguration(asNumber, idRouter, networks):
    networksList = []

    asNumber = Web3.toInt(int(asNumber))
    idRouter = Web3.toInt(int(idRouter))

    for network in networks.split(','):
        networksList.append(_intToHex(network))

    nonce = web3.eth.getTransactionCount(web3.eth.defaultAccount)
    
    web3.eth.setGasPriceStrategy(fast_gas_price_strategy)
    gasprice = web3.eth.generateGasPrice()
    
    txn_dict = contract.functions.addDeviceConfiguration(asNumber,idRouter, networksList).buildTransaction({
        'chainId': 3, 
        'gasPrice': gasprice,
        'nonce': nonce,
    })

    signed_txn = web3.eth.account.signTransaction(txn_dict, private_key=_getPK())
    result = web3.eth.sendRawTransaction(signed_txn.rawTransaction)

    print("Transaction hash: " + web3.toHex(result))

    tx_receipt = web3.eth.waitForTransactionReceipt(result)

    if tx_receipt is None:
        return ("Error: timeout")    
    else:
        if tx_receipt['status']:
            return "The transaction is a success."
        else:
            return "The transaction is a fail."

def delDeviceConfiguration(asNumber, idRouter):
    nonce = web3.eth.getTransactionCount(web3.eth.defaultAccount)

    asNumber = Web3.toInt(int(asNumber))
    idRouter = Web3.toInt(int(idRouter))
    
    web3.eth.setGasPriceStrategy(fast_gas_price_strategy)
    gasprice = web3.eth.generateGasPrice()
    
    txn_dict = contract.functions.delDeviceConfiguration(asNumber,idRouter).buildTransaction({
        'chainId': 3, 
        'gasPrice': gasprice,
        'nonce': nonce,
    })

    signed_txn = web3.eth.account.signTransaction(txn_dict, private_key=_getPK())
    result = web3.eth.sendRawTransaction(signed_txn.rawTransaction)

    print("Transaction hash: " + web3.toHex(result))

    tx_receipt = web3.eth.waitForTransactionReceipt(result)

    if tx_receipt is None:
        return ("Error: timeout")    
    else:
        if tx_receipt['status']:
            return "The transaction is a success."
        else:
            return "The transaction is a fail."

def getDeviceConfiguration(asNumber, idRouter):
    asNumber = Web3.toInt(int(asNumber))
    idRouter = Web3.toInt(int(idRouter))

    return contract.functions.getDeviceConfiguration(asNumber,idRouter).call()

def log_loop(event_filter, poll_interval, _asNumber, _idRouter):
    i = 1
    while True:
        for event in event_filter.get_new_entries():
            handle_event(event, _asNumber, _idRouter)
        time.sleep(poll_interval)
        print(i, file=sys.stderr)
        i =+ 1

def handle_event(event, _asNumber,_idRouter):
    if event['args']['_asNumber'] == int(_asNumber) and event['args']['_idRouter'] == int(_idRouter):
        print(event['args'], time.time())

if __name__=="__main__": 
    # compiled_sol = _compile_source_file("../contracts/BGPPrefixesManagerContract.sol")
    # contract_id, contract_interface = compiled_sol.popitem()
    # print(contract_id, contract_interface["abi"])
    # contract_id, contract_interface = compiled_sol.popitem()
    # print(contract_id, contract_interface["abi"])

    contract = web3.eth.contract(address=web3.toChecksumAddress(ISPConf.addressContract), abi=ISPConf.abiDescription)

    parser = argparse.ArgumentParser(prog= 'wrapperISP.py', description= 'It is a wrapper to interact with a smart contract for .')
    parser.add_argument('-c', '--addConf', nargs=3, help='', metavar=('AS Number,','ID Router,','List of Networks'))
    parser.add_argument('-d', '--delConf', nargs=2, help='', metavar=('AS Number,','ID Router'))
    parser.add_argument('-g', '--getConf', nargs=2, help='', metavar=('AS Number,','ID Router'))
    parser.add_argument('-e', '--handlerEvent', nargs=2, help='', metavar=('AS Number,','ID Router'))
    args = parser.parse_args()

    if args.addConf:
        print(addDeviceConfiguration(args.addConf[0], args.addConf[1], args.addConf[2]))
    elif args.delConf:
        print(delDeviceConfiguration(args.delConf[0], args.delConf[1]))
    elif args.getConf:
        print(getDeviceConfiguration(args.getConf[0], args.getConf[1]))
    elif args.handlerEvent:
        event_filter = contract.events.AddDeviceConf.createFilter(fromBlock=0)

        log_loop(event_filter, 1, args.handlerEvent[0], args.handlerEvent[1])
