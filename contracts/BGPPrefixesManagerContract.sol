// SPDX-License-Identifier: GPL-3.0
pragma solidity >=0.4.22 <0.7.0;
  
import "./LIRContract.sol";

contract BGPPrefixesManagerContract{
    event AddROA(uint16 _asNumber, bytes5 _prefix, uint8 _maxLen, uint _period);
    event DelROA(uint16 _asNumber, bytes5 _prefix, uint8 _maxLen);
    event AddDeviceConf(uint16 _asNumber, uint _idRouter, bytes5[] _prefixes);
    event DelDeviceConf(uint16 _asNumber, uint _idRouter);
    
    address public owner;
    
    enum Members {RIR,LIR,IPS}
    
    constructor() public{
        owner = msg.sender;
    }    struct deviceConfiguration{
        uint16 as_num;
        address owner;
        uint id_router;
        bytes5[] prefixes;
        bool status;
    }

    struct ROA {
        uint16 as_num;
        bytes5 prefix;
        uint8 max_len;
        uint start_time;
        uint period;
        bool status;
    }

    struct ISP {
        uint16 as_num;
        bool status;
    }

    mapping (bytes32 => ROA) roasList;
    mapping (bytes32 => mapping( bytes32 => deviceConfiguration)) devices;
    mapping (address => bool) RIRs;
    mapping (address => bool) LIRs;
    mapping (address => ISP) ISPes;

    modifier onlyOwner{
        require(
            msg.sender == owner,
            "Sender does not own this contract.");
        _;
    }

    modifier onlyRIRandLIR{
        require(
            RIRs[msg.sender] || LIRs[msg.sender],
            "Sender is not Regional or Local Registrator.");
        _;
    }

    modifier onlyISP{
        require(
            ISPes[msg.sender].status,
            "Sender is not ISP.");
        _;
    }

    modifier payment {
        require(
            msg.value > 0,
            "Payment is too low."
        );
        _;
    }

    function addRIRorLIR(address _memberAddress, Members _typeOfMember) public onlyOwner returns(bool) {
        if(_typeOfMember == Members.RIR) {
            RIRs[_memberAddress] = true;
            return true;
        } else if (_typeOfMember == Members.LIR) {
            LIRs[_memberAddress] = true;
            return true;
        }
        return false;
    }


    function isMember(address _memberAddress, Members _typeOfMember) public view returns(bool) {
        if(_typeOfMember == Members.RIR) {
            return RIRs[_memberAddress];
        } else if (_typeOfMember == Members.LIR) {
            return LIRs[_memberAddress];
        } else if (_typeOfMember == Members.IPS) {
            return ISPes[_memberAddress].status;
        }
    }

    function addISP(address _memberAddress, uint16 _asNumber) public onlyRIRandLIR returns(bool) {
        if(!ISPes[_memberAddress].status){
            ISPes[_memberAddress].as_num = _asNumber;
            ISPes[_memberAddress].status = true;
            return true;
        } else {
            revert("ISP have already existed.");
        }
    }

    function removeMember(address _memberAddress, Members _typeOfMember) public onlyOwner returns(bool) {
         if(_typeOfMember == Members.RIR && RIRs[_memberAddress] == true) {
            RIRs[_memberAddress] = false;
            return true;
        } else if (_typeOfMember == Members.LIR && LIRs[_memberAddress] == true) {
            LIRs[_memberAddress] = false;
            return true;
        } else if (_typeOfMember == Members.IPS && ISPes[_memberAddress].status == true) {
            ISPes[_memberAddress].status = false;
            return true;
        }
        return false;
    }

    function networkCheckFormat(bytes5 _network) internal pure returns (bool) {
        bytes5 pf = _network;
        bytes5 pos = hex"FFFFFFFFFF";
        bytes5 zeros = hex"0000000000";
        pos <<= (40 - uint8(_network[4]));
        pf = pf & pos;
        bytes5 mask = (zeros | _network[4]) >> 32;
        pf = pf | mask;

        if(pf != _network)
            return false;

        if(uint8(_network[4]) > 32)
            return false;

        return true;
    }

    function calculateHash(bytes memory barr) internal pure returns (bytes32) {
        return keccak256(barr);
    }

    function addROA(
        uint16 _asNumber,
        bytes5 _network,
        uint8 _maxLen,
        uint _period,
        address _LIR
    ) external onlyRIRandLIR returns(bool){
        bytes32 hashOfPrefix = calculateHash(abi.encodePacked(_network));

        if(!networkCheckFormat(_network)){
            revert("The format of ROA is incorrect.");
        }
        else if(roasList[hashOfPrefix].prefix == _network) {
            revert("The ROA has already existed.");
        }
        else if (uint8(_network[4]) > _maxLen){
            revert("The prefix is more specific than is allowed by the maximum length.");
        }
        else if (!LIRContract(_LIR).isPrefixMe(_network)){
           revert("The prefix is not owned by LIR!!!");
        }
        else {
            roasList[hashOfPrefix].as_num = _asNumber;
            roasList[hashOfPrefix].prefix = _network;
            roasList[hashOfPrefix].status = true;
            roasList[hashOfPrefix].period = _period;
            roasList[hashOfPrefix].max_len = _maxLen;
            roasList[hashOfPrefix].start_time = now;

            emit AddROA(_asNumber, _network, _maxLen, _period);
        }

        return true;
    }

    function delROA(
        uint16 _asNumber,
        bytes5 _network,
        uint8 _maxLen,
        address _LIR
    ) external onlyRIRandLIR returns(bool){
        bytes32 hashOfPrefix = calculateHash(abi.encodePacked(_network));

        if (!LIRContract(_LIR).isPrefixMe(_network)){
            revert("The prefix is not owned by LIR!!!");
        }
        else if(roasList[hashOfPrefix].prefix == _network &&
            roasList[hashOfPrefix].as_num == _asNumber &&
            roasList[hashOfPrefix].max_len == _maxLen){

            delete roasList[hashOfPrefix];

            emit DelROA(_asNumber, _network, _maxLen);
        }
    }

    function getROA(bytes5 _network) public view returns (uint16, bytes5, uint8, uint, uint, bool) {
        ROA memory roa = roasList[keccak256(abi.encodePacked((_network)))];
        bool valid;

        if(checkROAValidity(_network)){
            valid = true;
        }
        else{
            valid = false;
        }

        return (roa.as_num, roa.prefix, roa.max_len, roa.start_time, roa.period, valid);
    }

    function checkROA(bytes5 _prefix, uint16 _asNumber)internal view returns (bool){
        uint8 mask = uint8(_prefix[4]);
        bytes5 pf = _prefix;
        bytes5 pos = hex"FFFFFFFFFF";
        bytes5 zeros = hex"0000000000";
        pos <<= (40 - mask);

        for(uint8 i = 0; i < mask; i++){
            pf = pf & pos;
            bytes1 new_mask = bytes1(mask - i);
            bytes5 mask_arr = (zeros | new_mask) >> 32;
            pf = pf | mask_arr;
            bytes32 hashOfPrefix = calculateHash(abi.encodePacked(pf));

            if (roasList[hashOfPrefix].as_num == _asNumber){
                if((roasList[hashOfPrefix].max_len >= mask) && checkROAValidity(pf)){
                    return true;
                }
                else {
                    return false;
                }
            }
            pos <<= 1;
        }
        return false;
    }

    function checkROAValidity(bytes5 _prefix) internal view returns(bool){
        bytes32 hashOfPrefix = calculateHash(abi.encodePacked(_prefix));

        if((roasList[hashOfPrefix].start_time + roasList[hashOfPrefix].period) >= now){
            return true;
        }
        else{
            return false;
        }
    }  

    function addDeviceConfiguration(uint16 _asNumber, uint _idRouter,
            bytes5[] memory _prefixes) public onlyISP returns(bool) {

        if(ISPes[msg.sender].status == true && ISPes[msg.sender].as_num == _asNumber){
            bytes32 key_as = calculateHash(abi.encodePacked(bytes2(_asNumber)));
            bytes32 key_device = calculateHash(abi.encodePacked(bytes32(_idRouter)));
            devices[key_as][key_device].prefixes = new bytes5[](_prefixes.length);
            devices[key_as][key_device].as_num = _asNumber;
            devices[key_as][key_device].owner = msg.sender;
            devices[key_as][key_device].id_router = _idRouter;

            for(uint i; i < _prefixes.length; i++){
                if (checkROA(_prefixes[i], _asNumber)){
                    devices[key_as][key_device].prefixes[i] = _prefixes[i];
                }
                else {
                    revert('Some prefix is found in Roa list or is not valid.!!!');
                }
            }

            devices[key_as][key_device].status = true;
            emit AddDeviceConf(_asNumber, _idRouter, devices[key_as][key_device].prefixes);

            return true;
        }
        else{
            revert("Addition of the configuration is rejected." 
                    "AS number is not owned by Public Address!!!");
        }
    }

    function delDeviceConfiguration(uint16 _asNumber, uint _idRouter) public onlyISP returns(bool) {
        if(ISPes[msg.sender].status == true && ISPes[msg.sender].as_num == _asNumber){
            bytes32 key_as = calculateHash(abi.encodePacked(bytes2(_asNumber)));
            bytes32 key_device = calculateHash(abi.encodePacked(bytes32(_idRouter)));
            delete devices[key_as][key_device];
            emit DelDeviceConf(_asNumber, _idRouter);

            return true;
        }
        else {
            revert("Delete of configuration is rejected."
                "AS number is not owned by Public Address!!!");
        }
    }

    function getDeviceConfiguration(uint16 _asNumber, uint _idRouter) public view returns(bytes5[] memory){
        bytes32 key_as = calculateHash(abi.encodePacked(bytes2(_asNumber)));
        bytes32 key_device = calculateHash(abi.encodePacked(bytes32(_idRouter)));

        deviceConfiguration memory conf = devices[key_as][key_device];

        return conf.prefixes;
    }
}