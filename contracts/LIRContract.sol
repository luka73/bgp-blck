// SPDX-License-Identifier: GPL-3.0\npragma solidity >=0.4.22 <0.7.0;
pragma solidity >=0.4.22 <0.7.0;

import "./BGPPrefixesManagerContract.sol";

contract LIRContract {
    address public owner;
    address public addressLIR;
    address public addressBGPPMC;
    string public nameLIR;

    mapping(bytes5 => bool) prefixes;
    bytes5[] keysPrefixes;

    constructor (address _addressLIR, string memory _nameLIR, bytes5[] memory _prefixes, address _addressBGPPMC) public {
        owner = msg.sender;
        addressLIR = _addressLIR;
        nameLIR = _nameLIR;
        addressBGPPMC = _addressBGPPMC;
        for(uint i = 0; i < _prefixes.length; i++){
            prefixes[_prefixes[i]] = true;
            keysPrefixes.push(_prefixes[i]);
        }
    }

    modifier onlyLIR{
        require(
            msg.sender == addressLIR,
            "Sender is not ISP.");
        _;
    }

        function isPrefixMe(bytes5 _prefix) external view returns(bool) {
            uint8 mask = uint8(_prefix[4]);
            bytes5 pf = _prefix;
            bytes5 pos = hex"FFFFFFFFFF";
            bytes5 zeros = hex"0000000000";
            pos <<= (40 - mask);

            for(uint8 i = 0; i < mask; i++){
                pf = pf & pos;
                bytes1 new_mask = bytes1(mask - i);
                bytes5 mask_arr = (zeros | new_mask) >> 32;
                pf = pf | mask_arr;

                if (prefixes[pf]){
                    return true;
                }

                pos <<= 1;
            }
            return false;
        }

        function getPrefixes() public view returns(bytes5[] memory){
            return keysPrefixes;
        }

        function getOwner() public view returns(address){
            return owner;
        }

        function setROA(uint16 _asNumber, bytes5 _network, uint8 _maxLen, uint _period) public onlyLIR returns(bool){
            BGPPrefixesManagerContract bgpPMC = BGPPrefixesManagerContract(addressBGPPMC);

            return bgpPMC.addROA(_asNumber, _network, _maxLen, _period, address(this));
        }

        function removeROA(uint16 _asNumber, bytes5 _network, uint8 _maxLen) public onlyLIR returns(bool){
            BGPPrefixesManagerContract bgpPMC = BGPPrefixesManagerContract(addressBGPPMC);

            return bgpPMC.delROA(_asNumber, _network, _maxLen, address(this));
        }

        function addISP(address _addreesISP, uint16 _asNumber)public onlyLIR returns(bool){
            BGPPrefixesManagerContract bgpPMC = BGPPrefixesManagerContract(addressBGPPMC);

            return bgpPMC.addISP(_addreesISP,_asNumber);
            }
        }