// SPDX-License-Identifier: GPL-3.0
pragma solidity >=0.4.22 <0.7.0;

import "./LIRContract.sol";

contract RIRManagerContract {

    event DeployNewLIRContract(address _address);

    struct prefix {
        bytes5 network;
        bool status;
    }

    address public owner;
    address public addressBGPPMC;
    string public nameRIR;
    address[] keysLIRs;
    mapping(uint8 => mapping(bytes5 => prefix)) prefixes;
    mapping(address => address) LIRContracts;

    constructor(string memory _nameRIR, address _addressBGPPMC) public{
        owner = msg.sender;
        nameRIR = _nameRIR;
        addressBGPPMC = _addressBGPPMC;
    }

    modifier onlyOwner{
        require(
            msg.sender == owner,
            "Sender does not own this contract.");
        _;
    }

    function createLIRContract(address _addressLIR,
            string  memory  _nameLIR, bytes5[] memory _prefixes)public onlyOwner {
        address newLIRSC;
        if(LIRContracts[_addressLIR] != address(0)){
            revert("LIR contract have already existed!!!");
        }
        else{
            for(uint i = 0; i < _prefixes.length; i++){
                if(prefixes[uint8(_prefixes[i][4])][_prefixes[i]].status == true){
                    revert("IP prefix have already been assigned.");
                }
                else if(prefixes[uint8(_prefixes[i][4])][_prefixes[i]].network == bytes5(0x0)){
                    revert("IP prefix have not already been allocated.");
                }
                else {
                    prefixes[uint8(_prefixes[i][4])][_prefixes[i]].status = true;
                }
            }
            newLIRSC = address(new LIRContract(_addressLIR, _nameLIR, _prefixes, addressBGPPMC));
            keysLIRs.push(_addressLIR);
            LIRContracts[_addressLIR] = newLIRSC;

            emit DeployNewLIRContract(newLIRSC);
        }
    }

    function getLIRContract(address _addressLIR)public view returns(address) {
        if(LIRContracts[_addressLIR] == address(0x0)){
            revert("LIR contract is not existed for address!!!");
        }
        else{
            return LIRContracts[_addressLIR];
        }
    }

    function addPrefixPool(bytes5 _prefix)public onlyOwner returns(bool) {
        if(prefixes[uint8(_prefix[4])][_prefix].network != bytes5(0x0)){
            revert("IP prefix have already been allocated.");
        }
        else{
            prefixes[uint8(_prefix[4])][_prefix].status = false;
            prefixes[uint8(_prefix[4])][_prefix].network = _prefix;
            return true;
        }
    }

    function delPrefixPool(bytes5 _prefix)public onlyOwner returns(bool) {
        if(prefixes[uint8(_prefix[4])][_prefix].network == bytes5(0x0)){
            revert("IP prefix have not already been allocated.");
        }
        else{
            prefixes[uint8(_prefix[4])][_prefix].status = false;
            prefixes[uint8(_prefix[4])][_prefix].network = bytes5(0x0);
            return true;
        }
    }

    function getPrefixPool(bytes5 _prefix)public view returns(bytes5, bool) {
        if(prefixes[uint8(_prefix[4])][_prefix].network == bytes5(0x0)){
            revert("IP prefix have not already been allocated.");
        }
        return(prefixes[uint8(_prefix[4])][_prefix].network,
            prefixes[uint8(_prefix[4])][_prefix].status);
    }
}