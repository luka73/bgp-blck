# DATA ANALYSIS
## Prerequisites
1. Create python virtual environment (optional):
```
$ python -m venv /path/to/new/virtual/environment
$ cd /path/to/new/virtual/
$ . environment/bin/activate
```

2. Install required packages (mandatory):
```
(environment) $ pip install -r requirements.txt
```
## Time distribution of ROA setting and prefixes settings, Gas consumption
### Data resources:
1. transactions to set ROA -> [txs_roa](data/txs_roa.csv)
2. transactions to set prefixes -> [txs_config](data/txs_config.csv)
3. executed transactions with different number of prefixes -> [txs_with_different_ipPrefixCount](data/txs_with_different_ipPrefixCount.csv)
   
### Run analysis
1. Run jupyter-lab:
```
$  jupyter-lab
```

2. Browser opens jupiter-lab on http://localhost:8888/lab?token="" or http://127.0.0.1:8888/lab?token="" (token is viewed in CLI).

3. Select **[bgp20](bgp20.ipynb)** in the left menu.
   
4. Click on **Restart the kernel and run all cells**.
